import React from "react";
import home from "../../assets/home.svg";
import action from "../../assets/action.svg";
import user from "../../assets/user.svg";
import { Link } from "react-router-dom";

function Menu() {
  return (
    <div className="fixed inset-x-0 bottom-0 py-4 bg-white flex w-full justify-around">
      <Link to="/">
        <div className="flex flex-col justify-center items-center ">
          <img src={home} alt="home" className="w-[20px] h-auto" />
          <span className="text-sm">Trang chủ</span>
        </div>
      </Link>
      <Link to="/action">
        <div className="flex flex-col justify-center items-center">
          <img src={action} alt="action" className="w-[20px] h-auto" />
          <span className="text-sm">Hoạt động</span>
        </div>
      </Link>
      <Link to="/user">
        <div className="flex flex-col justify-center items-center">
          <img src={user} alt="user" className="w-[20px] h-auto" />
          <span className="text-sm">Tài khoản</span>
        </div>
      </Link>
    </div>
  );
}

export default Menu;
