import React from "react";
import { service } from "../../data/HomeData";

function Service() {
  return (
    <div className="flex justify-around w-full my-10">
      {service.map((item, index) => {
        return (
          <div key={item.id} className="p-3 bg-slate-50 border  rounded-full">
            <img
              src={`/${item.img}.svg`}
              alt="services"
              className="w-10 h-10"
            />
          </div>
        );
      })}
    </div>
  );
}

export default Service;
