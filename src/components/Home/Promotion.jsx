import React from "react";
import ScrollCarousel from "scroll-carousel-react";
import { promotion } from "../../data/HomeData";
function Promotion() {
  return (
    <div className="w-[95%] font-semibold">
      <h1 className="mb-4">Care tặng bạn nè</h1>
      <div className="flex flex-wrap gap-8 w-full justify-center ">
        {promotion.map((item, index) => {
          return (
            <div
              key={item.id}
              className="w-[40%] border border-gray-400 rounded-md overflow-hidden"
            >
              <img
                src={`/${item.img}.png`}
                alt="promotion"
                className="w-full h-20 object-"
              />
            </div>
          );
        })}
      </div>
      <h1 className="mb-4 mt-4">Care Dịch vụ</h1>
      <ScrollCarousel autoplay autoplaySpeed={1} speed={1}>
        {promotion.map((item, index) => {
          return (
            <div
              key={item.id}
              className="w-48 h-26 border border-gray-400 rounded-md overflow-hidden"
            >
              <img
                src={`/${item.img}.png`}
                alt="promotion"
                className="w-full object-cover"
              />
            </div>
          );
        })}
      </ScrollCarousel>
    </div>
  );
}

export default Promotion;
