import React, { useContext, useEffect, useState } from "react";
import { TripContext } from "../../context/TripContext";
import { BaseUrl, getRequest, postRequest } from "../../utils/services";

function TripInfor() {
  const { trip } = useContext(TripContext);
  const [inforDriver, setInforDriver] = useState();

  useEffect(() => {
    const getInforDriver = async (driverId) => {
      const res = await getRequest(`${BaseUrl}/users/find/${driverId}`);
      if (res.error) {
        return console.log(res);
      }
      setInforDriver(res.metadata);
    };

    if (trip) {
      getInforDriver(trip.driverId);
    }
  }, [trip]);
  return trip ? (
    <div className="absolute inset-x-0 bottom-0 z-20 bg-white px-3 py-5">
      <div>
        <h1 className="text-md font-semibold pb-2 border-b-2 border-indigo-600">
          Chúc bạn chuyến đi an toàn vui vẻ
        </h1>
      </div>
      <div className="flex items-center gap-3 mt-3 py-4">
        <div>
          <img src="/driver.svg" alt="avatar customer" className="w-16 h-16" />
        </div>
        <div className="flex justify-between grow items-center">
          <div className="flex flex-col gap-1">
            <p className="font-semibold text-xl">{inforDriver?.name}</p>
            <p className="font-semibold text-xl p-2 bg-slate-800 text-white rounded-md">
              {inforDriver?.license_plate}
            </p>
          </div>
          <div className="flex flex-col p-4">
            <span>
              <img src="/message.svg" alt="Chat" className="w-8 h-8" />
            </span>
          </div>
        </div>
      </div>
      <div className="w-full flex justify-center mt-5">
        <div className="w-[98%] pb-3 rounded-sm">
          <div className="text-center">
            <button className="text-xl font-semibold w-[95%] bg-blue-100 p-3 rounded-md">
              Gọi tài xế
            </button>
          </div>
        </div>
      </div>
    </div>
  ) : null;
}

export default TripInfor;
