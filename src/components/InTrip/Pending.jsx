import React, { useContext, useState, useEffect } from "react";
import { TripContext } from "../../context/TripContext";

function Pending() {
  const { trip, tripUpdateStatus, setIsAllDriverBusy } =
    useContext(TripContext);
  const [countdown, setCountdown] = useState(20);
  const [isActive, setIsActive] = useState(true);

  useEffect(() => {
    let interval;
    if (isActive && countdown > 0) {
      interval = setInterval(() => {
        setCountdown((prev) => prev - 1);
      }, 1000);
    } else if (countdown === 0) {
      setIsActive(false);
      setIsAllDriverBusy(true);
      tripUpdateStatus("cancel");
    }

    return () => clearInterval(interval);
  }, [isActive, countdown]);

  return trip ? (
    <div className="w-full absolute inset-x-0 bottom-0 z-20">
      <div className="w-full flex flex-col items-center gap-2">
        <p className="w-[90%] p-3 bg-white rounded-md whitespace-nowrap overflow-hidden text-ellipsis text-sm">
          {trip.source}
        </p>
        <p className="w-[90%] p-3 bg-white rounded-md whitespace-nowrap overflow-hidden text-ellipsis text-sm">
          {trip.destination}
        </p>
      </div>
      <div className="mt-5 flex justify-center bg-white border-t-2 border-gray-200 flex-col items-center">
        <p className="p-3 w-[80%] bg-blue-200 text-blue-500 font-semibold text-center">
          Đang tìm tài xế...
        </p>
        <h1
          className="py-5 font-semibold text-xl"
          onClick={() => tripUpdateStatus("cancel")}
        >
          Hủy chuyến ({countdown}s)
        </h1>
      </div>
    </div>
  ) : null;
}

export default Pending;
