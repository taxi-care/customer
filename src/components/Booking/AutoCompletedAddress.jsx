import React, { useState, useEffect, useContext } from "react";
import { TripContext } from "../../context/TripContext";

function AutoCompletedAddress() {
  const [sourceChange, setSourceChange] = useState(false);
  const [destinationChange, setDestinationChange] = useState(false);
  const [addressList, setAddressList] = useState([]);
  const {
    setSourceCoordinates,
    setDestinationCoordinates,
    source,
    setSource,
    destination,
    setDestination,
  } = useContext(TripContext);

  useEffect(() => {
    const deplayDebounceFn = setTimeout(() => {
      getAddressList();
    }, 1000);

    return () => clearTimeout(deplayDebounceFn);
  }, [source, destination]);

  const getAddressList = async () => {
    setAddressList([]);
    if (source || destination) {
      const query = sourceChange ? source : destination;
      if (query) {
        const res = await fetch(
          `https://maps.vietmap.vn/api/autocomplete/v3?apikey=4984c36970bf2ee8648026e6bc029b6ec3766ba2fbcbea4f&text=${query}`
        );

        const result = await res.json();
        setAddressList(result);
      }
    }
  };

  const onSourceAddressClick = async (item) => {
    setSource(item.display);
    setAddressList([]);
    setSourceChange(false);
    const res = await fetch(
      `https://maps.vietmap.vn/api/place/v3?apikey=4984c36970bf2ee8648026e6bc029b6ec3766ba2fbcbea4f&refid=${item.ref_id}`
    );
    const result = await res.json();
    setSourceCoordinates({
      lat: result.lat,
      lng: result.lng,
    });
  };
  const onDestinationAddressClick = async (item) => {
    setDestination(item.display);
    setAddressList([]);
    setDestinationChange(false);
    const res = await fetch(
      `https://maps.vietmap.vn/api/place/v3?apikey=4984c36970bf2ee8648026e6bc029b6ec3766ba2fbcbea4f&refid=${item.ref_id}`
    );

    const result = await res.json();

    setDestinationCoordinates({
      lat: result.lat,
      lng: result.lng,
    });
  };
  return (
    <div className="mt-1 w-[90%]  mx-auto">
      <div className="relative">
        <input
          type="text"
          className="bg-white border-[1px] w-full rounded-md outline-none focus:border-blue-300 text-sm p-2"
          value={source}
          onChange={(e) => {
            setSource(e.target.value);
            setSourceChange(true);
          }}
          placeholder="Where to?"
        />
        {addressList.length > 0 && sourceChange ? (
          <div
            className="shadow-md p-1 rounded-md
          absolute w-full bg-white z-20 h-[300px] overflow-auto"
          >
            {addressList?.map((item, index) => (
              <h2
                key={index}
                className="p-3 hover:bg-gray-100 cursor-pointer"
                onClick={() => {
                  onSourceAddressClick(item);
                }}
              >
                {item.display}
              </h2>
            ))}
          </div>
        ) : null}
      </div>
      <div className="my-1">
        <img src="/twoArrow.svg" alt="two-way arrow" className="w-5 h-5" />
      </div>
      <div className="relative">
        <input
          type="text"
          className="bg-white p-2 border-[1px] w-full rounded-md outline-none focus:border-blue-300 text-sm"
          value={destination}
          onChange={(e) => {
            setDestination(e.target.value);
            setDestinationChange(true);
          }}
          placeholder="Where from?"
        />
        {addressList.length > 0 && destinationChange ? (
          <div className="shadow-md p-1 rounded-md absolute w-full bg-white z-19 h-[300px] overflow-auto">
            {addressList?.map((item, index) => (
              <h2
                key={index}
                className="p-3 hover:bg-gray-100 cursor-pointer"
                onClick={() => {
                  onDestinationAddressClick(item);
                }}
              >
                {item.display}
              </h2>
            ))}
          </div>
        ) : null}
      </div>
    </div>
  );
}

export default AutoCompletedAddress;
