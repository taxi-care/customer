export const promotion = [
  {
    id: 1,
    img: "pro1",
    desciption: "Mở tài khoản,nhận nhận ngàn ưu đãi",
  },
  {
    id: 2,
    img: "pro2",
    desciption: "CareBike ưu đãi 50%",
  },
  {
    id: 3,
    img: "pro3",
    desciption: "Viettel Money tặng bạn 40k",
  },
  {
    id: 4,
    img: "pro4",
    desciption: "Ưu đãi bạn mới đến 50k",
  },
];

export const service = [
  {
    id: 1,
    name: "Giao đồ ăn",
    img: "delivery",
  },
  {
    id: 2,
    name: "Giao hàng",
    img: "delivery1",
  },
  {
    id: 3,
    name: "Xe máy",
    img: "bike1",
  },
  {
    id: 4,
    name: "Ô tô",
    img: "Car",
  },
];
