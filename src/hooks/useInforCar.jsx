import { useEffect, useState } from "react";
import { BaseUrl, getRequest } from "../utils/services";

export const useInforCar = (trip) => {
  const [inforCar, setInforCar] = useState();
  useEffect(() => {
    const getInforCar = async () => {
      const res = await getRequest(`${BaseUrl}/cars/${trip.selectedCar}`);
      if (res.error) {
        return console.log(res);
      }
      setInforCar(res.metadata);
    };
    getInforCar();
  }, [trip]);
  return { inforCar };
};
