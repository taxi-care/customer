import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";
import { Link } from "react-router-dom";
import Logo from "../assets/Logo.png";

function Register() {
  const {
    registerInfor,
    updateRegisterInfor,
    registerUser,
    registerError,
    isRegisterLoading,
  } = useContext(AuthContext);
  return (
    <div className="flex flex-col items-center h-full relative ">
      <div className="flex justify-center mt-[20%] md:mt-[5%]">
        <img src={Logo} alt="logo" className="w-[200px] h-auto" />
      </div>
      <h1 className="text-center my-10 text-3xl">Sign up</h1>
      <div className="flex flex-col gap-5 w-[90%]">
        <input
          type="text"
          className="bg-white p-1 border-[1px] w-full rounded-full outline-none focus:border-[#3422F2] py-3 px-4 text-sm"
          placeholder="Name"
          onChange={(e) =>
            updateRegisterInfor({ ...registerInfor, name: e.target.value })
          }
        />
        <input
          type="text"
          className="bg-white p-1 border-[1px] w-full rounded-full outline-none focus:border-[#3422F2] py-3 px-4 text-sm"
          placeholder="Phone"
          onChange={(e) =>
            updateRegisterInfor({ ...registerInfor, phone: e.target.value })
          }
        />
        <input
          type="password"
          className="bg-white p-1 border-[1px] w-full rounded-full outline-none focus:border-[#3422F2] py-3 px-4 text-sm"
          placeholder="Password"
          onChange={(e) =>
            updateRegisterInfor({ ...registerInfor, password: e.target.value })
          }
        />
      </div>
      {registerError?.error && (
        <div className="mt-8 w-[90%]">
          <p className="w-full text-red-500 py-3">{registerError?.message}</p>
        </div>
      )}
      <div className="mt-8 w-[90%]">
        <button
          className="w-full bg-[#3422F2] text-white py-3 rounded-full"
          disabled={isRegisterLoading}
          onClick={registerUser}
        >
          {isRegisterLoading ? "Creating your account" : "Sign up"}
        </button>
      </div>
      <p className="mt-5">
        Already have an account?
        <Link to="/login">
          <span className="cursor-pointer text-blue-600"> Sign In</span>
        </Link>
      </p>
    </div>
  );
}

export default Register;
