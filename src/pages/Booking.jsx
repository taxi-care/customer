import React, { useContext, useEffect, useState } from "react";
import AutoCompletedAddress from "../components/Booking/AutoCompletedAddress";
import MapboxMap from "../components/Map/MapboxMap";
import { useNavigate } from "react-router-dom";
import { TripContext } from "../context/TripContext";
import Vehicle from "../components/Booking/Vehicle";

function Booking() {
  const navigate = useNavigate();
  const {
    setSourceCoordinates,
    setDestinationCoordinates,
    setDirectionData,
    setSource,
    setDestination,
    tripRequest,
    isAllDriverBusy,
    source,
    sourceCoordinates,
    destination,
    destinationCoordinates,
    selectedCar,
    directionData,
  } = useContext(TripContext);
  const handleClickBack = () => {
    setSourceCoordinates(null);
    setDestinationCoordinates(null);
    setSource("");
    setDestination("");
    setDirectionData([]);
    navigate("/");
  };
  const [isReadyBooking, setIsReadyBooking] = useState(false);
  useEffect(() => {
    if (
      source &&
      sourceCoordinates &&
      destination &&
      destinationCoordinates &&
      selectedCar &&
      directionData
    ) {
      setIsReadyBooking(true);
    }
  }, [
    source,
    sourceCoordinates,
    destination,
    destinationCoordinates,
    selectedCar,
    directionData,
  ]);
  return (
    <div className="w-screen h-screen relative">
      <MapboxMap />
      <div className="absolute z-20 w-full">
        <div
          className="mt-4 ml-3 p-3 bg-slate-100 rounded-md inline-block "
          onClick={handleClickBack}
        >
          <img src="/back.svg" alt="back" className="w-[15px] h-[15px]" />
        </div>
        <AutoCompletedAddress />
      </div>
      <div className="absolute z-20 w-full inset-x-0 bottom-0 bg-white pt-4 rounded-t-2xl">
        {isAllDriverBusy ? (
          <div className="mx-auto w-[95%] text-red-500">
            Tất cả tài xế đang bận, vui lòng thử lại sau
          </div>
        ) : null}
        <Vehicle />
        <div className="mx-auto w-[95%] flex my-5 gap-5 text-lg font-semibold">
          <h1>Distance: </h1>
          <p>
            {directionData?.routes
              ? `${(directionData.routes[0].distance / 1000).toFixed(1)} km`
              : null}
          </p>
        </div>
        <div className="mx-auto w-[95%] my-2" onClick={() => tripRequest()}>
          <button
            className={`w-full
            ${
              isReadyBooking
                ? "bg-blue-700 text-white"
                : "bg-slate-400 text-black"
            }
           p-2 rounded-md `}
            disabled={isReadyBooking ? false : true}
          >
            Book
          </button>
        </div>
      </div>
    </div>
  );
}

export default Booking;
