import { Outlet } from "react-router-dom";
import MapboxMap from "../components/Map/MapboxMap";
import Menu from "../components/Home/Menu";

function Home() {
  return (
    <div className="w-screen h-screen bg-gradient-to-b from-indigo-400 relative inline-block">
      <Outlet />
      <Menu />
    </div>
  );
}

export default Home;
