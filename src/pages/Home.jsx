import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";
import Promotion from "../components/Home/Promotion";
import Service from "../components/Home/Service";

function Home() {
  const { user } = useContext(AuthContext);
  const navigate = useNavigate();
  return (
    <div className="flex flex-col items-center">
      <p className="mt-10 w-[95%] font-semibold">Chào {user?.name}!</p>
      <div className="w-[95%] mt-4">
        <input
          type="text"
          className="bg-white p-1 border-[1px] w-full rounded-md outline-none focus:border-[#3422F2] py-3 px-4 text-sm"
          placeholder="Bạn muốn tôi đưa đến nơi nào?"
          onClick={() => navigate("/booking")}
        />
      </div>
      <Service />
      <Promotion />
    </div>
  );
}

export default Home;
