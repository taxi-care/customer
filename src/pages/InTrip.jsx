import React, { useContext, useLayoutEffect } from "react";
import { TripContext } from "../context/TripContext";
import MapboxMap from "../components/Map/MapboxMap";
import Pending from "../components/InTrip/Pending";
import { useNavigate } from "react-router-dom";
import TripInfor from "../components/InTrip/TripInfor";
function InTrip() {
  const navigate = useNavigate();
  const { trip } = useContext(TripContext);
  useLayoutEffect(() => {
    if (!trip) navigate("/booking");
  }, [trip]);
  return (
    <div className="w-screen h-screen relative">
      <MapboxMap />
      {trip?.status == "pending" ? <Pending /> : null}
      {trip?.status == "intrip" ? <TripInfor /> : null}
    </div>
  );
}

export default InTrip;
