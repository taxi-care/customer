import React, { useContext, useEffect, useState } from "react";
import { BaseUrl, getRequest, postRequest } from "../utils/services";
import { AuthContext } from "../context/AuthContext";
import TripEnd from "../components/Action/TripEnd";

function Action() {
  const { user } = useContext(AuthContext);
  const [tripsHistory, setTripsHistory] = useState();
  useEffect(() => {
    const getTripsHistory = async () => {
      const res = await postRequest(
        `${BaseUrl}/trips/get-all-trip`,
        JSON.stringify({ userId: user?._id })
      );
      if (res.error) {
        return console.log(res);
      }
      setTripsHistory(res.metadata);
    };
    getTripsHistory();
  }, []);
  return (
    <div className="mt-4">
      <div>
        <h1 className="text-xl font-semibold ml-3">Hoạt động</h1>
        <div className="mt-3 flex flex-col items-center gap-4 pb-[120px]">
          {tripsHistory ? (
            tripsHistory?.map((item, index) => {
              return <TripEnd key={index} trip={item} />;
            })
          ) : (
            <h1 className="text-xl">Chưa có chuyến đi nào</h1>
          )}
        </div>
      </div>
    </div>
  );
}

export default Action;
