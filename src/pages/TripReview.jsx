import React, { useContext, useState, useEffect } from "react";
import { TripContext } from "../context/TripContext";
import { BaseUrl, getRequest } from "../utils/services";
import { Link, useNavigate } from "react-router-dom";

function TripReview() {
  const { tripReview, setTripReview } = useContext(TripContext);
  const [inforDriver, setInforDriver] = useState();
  const [selectedTip, setSelectedTip] = useState(null);
  const navigate = useNavigate();

  const handleTipSelection = (tipAmount) => {
    setSelectedTip(tipAmount);
  };
  useEffect(() => {
    const getInforDriver = async (driverId) => {
      const res = await getRequest(`${BaseUrl}/users/find/${driverId}`);
      if (res.error) {
        return console.log(res);
      }
      setInforDriver(res.metadata);
    };

    if (tripReview) {
      getInforDriver(tripReview.trip.driverId);
    }
  }, [tripReview]);
  useEffect(() => {
    if (!tripReview) {
      navigate("/");
    }
  }, [tripReview, navigate]);
  return (
    <div className="flex flex-col items-center bg-slate-100 h-screen relative">
      <Link to="/" onClick={() => setTripReview()}>
        <div className="absolute top-5 left-5 p-2 border border-gray-600 rounded-md">
          <img src="/back.svg" alt="back to home" className="w-6 h-6" />
        </div>
      </Link>
      <h1 className="text-2xl mt-10">Rating</h1>
      <div className="w-[95%] rounded-md bg-white mt-10 flex flex-col items-center p-3 gap-2">
        <img src="/DriverProfile.svg" alt="" className="w-16 h-16" />
        <p className="font-semibold">{inforDriver?.name}</p>
        <p>{inforDriver?.license_plate}</p>
        <h1 className="font-semibold text-xl mt-5">
          How was your trip with {inforDriver?.name}?
        </h1>
        <input
          type="text"
          className="w-[90%] rounded-full p-3 border border-gray-400"
          placeholder="Additional comments"
        />
        <p className="text-lg mt-5">Add a tip to {inforDriver?.name}</p>
        <div className="flex gap-5 mt-2">
          <button
            className={`rounded-full px-3 py-2 ${
              selectedTip === 10
                ? "bg-blue-500 text-white"
                : "bg-gray-300 text-gray-700"
            }`}
            onClick={() => handleTipSelection(10)}
          >
            $10
          </button>
          <button
            className={`rounded-full px-3 py-2 ${
              selectedTip === 20
                ? "bg-blue-500 text-white"
                : "bg-gray-300 text-gray-700"
            }`}
            onClick={() => handleTipSelection(20)}
          >
            $20
          </button>
          <button
            className={`rounded-full px-3 py-2 ${
              selectedTip === 30
                ? "bg-blue-500 text-white"
                : "bg-gray-300 text-gray-700"
            }`}
            onClick={() => handleTipSelection(30)}
          >
            $30
          </button>
        </div>
        <Link
          to="/"
          className="w-full flex justify-center"
          onClick={() => setTripReview()}
        >
          <button className="w-[90%] p-3 rounded-full bg-blue-700 text-white mt-3 mb-5">
            Submit Review
          </button>
        </Link>
      </div>
    </div>
  );
}

export default TripReview;
