import { Routes, Route, Navigate } from "react-router-dom";
import Index from "./pages/Index";
import Register from "./pages/Register";
import Login from "./pages/Login";
import { useContext } from "react";
import { AuthContext } from "./context/AuthContext";
import Action from "./pages/Action";
import User from "./pages/User";
import Home from "./pages/Home";
import Booking from "./pages/Booking";
import { TripContextProvider } from "./context/TripContext.jsx";
import InTrip from "./pages/InTrip";
import TripReview from "./pages/TripReview.jsx";

function App() {
  const { user } = useContext(AuthContext);
  return (
    <TripContextProvider user={user}>
      <Routes>
        <Route path="/" element={user ? <Index /> : <Login />}>
          <Route path="/" element={<Home />} />
          <Route path="/action" element={<Action />} />
          <Route path="/user" element={<User />} />
        </Route>
        <Route
          path="/register"
          element={user ? <Navigate to="/" /> : <Register />}
        />
        <Route path="/login" element={user ? <Navigate to="/" /> : <Login />} />
        <Route
          path="/booking"
          element={user ? <Booking /> : <Navigate to="/login" />}
        />
        <Route
          path="/intrip"
          element={user ? <InTrip /> : <Navigate to="/login" />}
        />
        <Route
          path="/trip-review"
          element={user ? <TripReview /> : <Navigate to="/login" />}
        />
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </TripContextProvider>
  );
}

export default App;
