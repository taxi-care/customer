import { createContext, useState, useEffect, useCallback } from "react";
import { io } from "socket.io-client";
import { BaseUrl, getRequest, postRequest } from "../utils/services";
import { useNavigate } from "react-router-dom";
import { useRef } from "react";

export const TripContext = createContext();

export const TripContextProvider = ({ children, user }) => {
  const navigate = useNavigate();

  const [userLocation, setUserLocation] = useState();
  const [source, setSource] = useState("");
  const [sourceCoordinates, setSourceCoordinates] = useState();
  const [destination, setDestination] = useState("");
  const [destinationCoordinates, setDestinationCoordinates] = useState();
  const [selectedCar, setSelectedCar] = useState();
  const [directionData, setDirectionData] = useState([]);
  const [trip, setTrip] = useState();
  const [socket, setSocket] = useState(null);
  const [isAllDriverBusy, setIsAllDriverBusy] = useState(false);
  const [driverLocation, setDriverLocation] = useState();
  const [tripReview, setTripReview] = useState();
  const [driverCancelTrip, setDriverCancelTrip] = useState(false);
  const socketRef = useRef(null);
  //Notification All Driver Busy
  useEffect(() => {
    if (isAllDriverBusy) {
      const timer = setTimeout(() => {
        setIsAllDriverBusy(false);
      }, 5000);
      return () => clearTimeout(timer);
    }
  }, [isAllDriverBusy]);

  //Function Request trip
  const tripRequest = useCallback(async () => {
    const res = await postRequest(
      `${BaseUrl}/trips/create`,
      JSON.stringify({
        source,
        sourceCoordinates: [sourceCoordinates.lng, sourceCoordinates.lat],
        destination,
        destinationCoordinates: [
          destinationCoordinates.lng,
          destinationCoordinates.lat,
        ],
        PhoneCustomer: user?.phone,
        NameCustomer: user?.name,
        distance: (directionData.routes[0].distance / 1000).toFixed(1),
        price: (
          (directionData.routes[0].distance / 1000) *
          selectedCar.price
        ).toFixed(1),
        userId: user?._id,
        selectedCar: selectedCar.id,
      })
    );

    if (res.error) {
      return console.log(res);
    }
    setTrip(res.trip);
  }, [
    source,
    sourceCoordinates,
    destination,
    destinationCoordinates,
    selectedCar,
    directionData,
  ]);

  //User Have Trip
  useEffect(() => {
    if (trip) {
      setSourceCoordinates({
        lng: trip.sourceCoordinates[0],
        lat: trip.sourceCoordinates[1],
      });
      setDestinationCoordinates({
        lng: trip.destinationCoordinates[0],
        lat: trip.destinationCoordinates[1],
      });
      navigate("/intrip");
    }
  }, [trip]);

  const tripUpdateStatus = useCallback(
    async (newStatus) => {
      if (trip) {
        const res = await postRequest(
          `${BaseUrl}/trips/update-status`,
          JSON.stringify({ tripId: trip._id, newStatus })
        );
        if (res.error) {
          return console.log(res);
        }
        if (newStatus == "cancel") {
          setTrip();
        }
      }
    },
    [trip]
  );

  useEffect(() => {
    const fetchData = async () => {
      if (user) {
        const res = await postRequest(
          `${BaseUrl}/trips/user-have-trip`,
          JSON.stringify({ userId: user?._id })
        );
        if (res.error) {
          return console.log(res);
        }
        if (res.trips.length > 0) {
          setTrip(res.trips[0]);
        }
      }
    };

    fetchData();
  }, [user]);

  //Init socket
  useEffect(() => {
    let attempts = 0;

    const connectSocket = () => {
      socketRef.current = io("http://localhost:8000");

      socketRef.current.on("connect", () => {
        console.log("Kết nối socket thành công");
        setSocket(socketRef.current);
        console.log(socketRef.current);
        attempts = 0;
      });

      socketRef.current.on("connect_error", () => {
        console.log("Kết nối socket thất bại, đang thử lại...");
        attempts++;
        if (attempts < 5) {
          setTimeout(connectSocket, 1000); // Thử lại sau 1 giây
        }
      });
    };

    if (user) {
      connectSocket();
    }
    return () => {
      socketRef?.current?.disconnect();
    };
  }, [user]);

  //Add online users
  useEffect(() => {
    if (socket === null) {
      return;
    }
    socket.emit("addNewUser", {
      userId: user?._id,
      role: "customer",
    });
  }, [socket]);

  //Listen driver accept trip and location driver realtime
  useEffect(() => {
    if (socket === null) {
      return;
    }
    socket.on("acceptTrip", (res) => {
      setTrip(res?.metadata);
      console.log("Đã tìm thấy tài xế les't go", res?.metadata);
    });
    socket.on("locationDriverInTrip", (res) => {
      setDriverLocation(res);
      console.log(res);
    });

    socket.on("driverCancelTrip", (res) => {
      setTrip(res);
      setDriverLocation();
      setDriverCancelTrip(true);
      console.log(
        "Tài xế mới hủy chuyến vui lòng đợi hệ thống tìm tài xế khác::",
        res
      );
    });

    socket.on("completedTrip", (res) => {
      setTrip();
      setSource("");
      setSourceCoordinates();
      setDestination("");
      setDestinationCoordinates();
      setTripReview(res);
      setDirectionData();
      setDriverLocation();
      setSelectedCar();
      navigate("/");
      console.log("chuyến đi đã hoàn thành hẹn gặp lại bạn nhé", res);
    });

    return () => {
      socket.off("acceptTrip");
      socket.off("locationDriverInTrip");
      socket.off("completedTrip");
      socket.off("driverCancelTrip");
    };
  }, [socket]);

  useEffect(() => {
    if (tripReview) {
      navigate("/trip-review");
    }
  }, [tripReview]);
  //Get user Location
  useEffect(() => {
    const getUserLocation = () => {
      navigator.geolocation.getCurrentPosition(function (pos) {
        setUserLocation({
          lat: pos.coords.latitude,
          lng: pos.coords.longitude,
        });
      });
    };
    getUserLocation();
  }, []);

  //Get driver Location in trip
  useEffect(() => {
    const getDriverLocationCurrentTrip = async (driverId) => {
      const res = await getRequest(`${BaseUrl}/driver-location/${driverId}`);
      if (res.error) {
        return console.log(res);
      }
      setDriverLocation({
        lng: res.metadata.currentLocation.coordinates[0],
        lat: res.metadata.currentLocation.coordinates[1],
      });
    };
    if (trip?.driverId) {
      getDriverLocationCurrentTrip(trip.driverId);
    }
  }, [trip]);

  return (
    <TripContext.Provider
      value={{
        userLocation,
        sourceCoordinates,
        setSourceCoordinates,
        destinationCoordinates,
        setDestinationCoordinates,
        directionData,
        setDirectionData,
        source,
        setSource,
        destination,
        setDestination,
        selectedCar,
        setSelectedCar,
        trip,
        tripRequest,
        tripUpdateStatus,
        isAllDriverBusy,
        setIsAllDriverBusy,
        driverLocation,
        tripReview,
        setTripReview,
      }}
    >
      {children}
    </TripContext.Provider>
  );
};
