import { createContext, useCallback, useEffect, useState } from "react";
import { BaseUrl, getRequestAuth, postRequest } from "../utils/services";

export const AuthContext = createContext();

export const AuthContextProvider = ({ children }) => {
  //Init User
  const [user, setUser] = useState();
  //Register
  const [registerError, setRegisterError] = useState(null);
  const [isRegisterLoading, setIsRegisterLoading] = useState(false);
  const [registerInfor, setRegisterInfor] = useState({
    name: "",
    phone: "",
    password: "",
  });
  //Login
  const [loginError, setLoginError] = useState(null);
  const [isLoginLoading, setIsLoginLoading] = useState(false);
  const [loginInfor, setIsLoginInfor] = useState({
    phone: "",
    password: "",
  });

  //Duy trì đăng nhập với access và refresh token
  useEffect(() => {
    const getUserWithToken = async () => {
      const response = await getRequestAuth(`${BaseUrl}/users/`);
      if (response.error) {
        return console.log(response);
      }
      setUser(response.metadata);
    };
    getUserWithToken();
  }, []);
  //Function Register
  const updateRegisterInfor = useCallback((infor) => {
    setRegisterInfor(infor);
  }, []);

  const registerUser = useCallback(async () => {
    setIsRegisterLoading(true);
    setRegisterError(null);

    const res = await postRequest(
      `${BaseUrl}/users/register`,
      JSON.stringify(registerInfor)
    );
    setIsRegisterLoading(false);

    if (res.error) {
      return setRegisterError(res);
    }
    localStorage.setItem("User", JSON.stringify(res.metadata));
    setUser(res.metadata);
  }, [registerInfor]);

  //Function Login
  const updateLoginInfor = useCallback((infor) => {
    setIsLoginInfor(infor);
  }, []);

  const loginUser = useCallback(async () => {
    setIsLoginLoading(true);
    setLoginError(null);

    const res = await postRequest(
      `${BaseUrl}/users/login/customer`,
      JSON.stringify(loginInfor)
    );
    setIsLoginLoading(false);

    if (res.error) {
      return setLoginError(res);
    }
    localStorage.setItem(
      "access_token",
      JSON.stringify(res.metadata.access_token)
    );
    localStorage.setItem(
      "refresh_token",
      JSON.stringify(res.metadata.refresh_token)
    );
    const currentUser = await getRequestAuth(`${BaseUrl}/users/`);
    if (currentUser.error) {
      return setLoginError(res);
    }
    setUser(currentUser.metadata);
  }, [loginInfor]);

  const logoutUser = useCallback(() => {
    localStorage.removeItem("access_token");
    localStorage.removeItem("refresh_token");
    setUser(null);
  }, []);

  return (
    <AuthContext.Provider
      value={{
        user,
        registerInfor,
        updateRegisterInfor,
        registerUser,
        registerError,
        isRegisterLoading,
        loginInfor,
        updateLoginInfor,
        loginUser,
        loginError,
        isLoginLoading,
        logoutUser,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
